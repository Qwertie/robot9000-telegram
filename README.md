# Robot9000 telegram

Telegram bot which implements most of XKCD's robot9000 https://blog.xkcd.com/2008/01/14/robot9000-and-xkcd-signal-attacking-noise-in-chat/

# Usage

`docker build -t robot9000 .`

`docker run -e BOT_TOKEN= -v '/path/to/db/on/host/test.sqlite3':/robot9000/db/test.sqlite3:Z`

Now invite it to a group and give it admin permissions. Should just work 👍
