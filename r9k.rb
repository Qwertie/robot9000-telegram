require 'telegram/bot'
require 'awesome_print'
require './models/message'

token = ENV['BOT_TOKEN']
ActiveRecord::Base.establish_connection(
  YAML.load(File.open('config/database.yml'))
)

# Returns true if the message has never been seen before in group
def message_original?(api_message)
  !Message.exists?(stripped_message: sanitize_message(api_message[:text]))
end

# Converts messages in to a more simple form without punctuation and other unrelated marks
def sanitize_message(message_string)
  return '' if message_string.nil? || !message_string.is_a?(String)

  replacements = { 'á' => 'a', 'ë' => 'e' }

  message_string.encode(
    Encoding.find('ASCII'),
    invalid: :replace, # Replace invalid byte sequences
    replace: '', # Use a blank for those replacements
    universal_newline: true, # Always break lines with \nruby
    # For any character that isn't defined in ASCII, run this
    # code to find out how to replace it
    fallback:
      lambda do |char|
        # If no replacement is specified, use an empty string
        replacements.fetch(char, '')
      end
  ).downcase
end

Telegram::Bot::Client.run(token) do |bot|
  bot.listen do |message|
    # The telegram api library likes raising exceptions whenever a request is rejected for any reason
    begin
      if message&.text&.is_a?(String)
        if message.text == '/start'
          bot.api.send_message(chat_id: message.chat.id, text: "Hello, #{message.from.first_name}")
        elsif message.text == '/help'
          bot.api.send_message(chat_id: message.chat.id, text: "This bot can be added to a telegram group and when given admin access, it will delete any message it has seen before")
        elsif message_original?(message)
          Message.create(
            original_message: message[:text],
            stripped_message: sanitize_message(message[:text]),
            group_id: message[:chat][:id],
            user_id: message.from.id,
            user_name: message.from.username
          )
        else
          bot.api.deleteMessage(
            chat_id: message.chat.id,
            message_id: message.message_id
          )
        end
      end
    rescue Telegram::Bot::Exceptions::ResponseError => e
      puts e.message
      puts e.backtrace.inspect
    end
  end
end
