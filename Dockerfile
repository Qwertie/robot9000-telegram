FROM ruby:3.0.0

ADD . /robot9000

RUN cd /robot9000 && \
    bundle install

CMD cd /robot9000 && bundle exec ruby r9k.rb
