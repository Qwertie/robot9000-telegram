class AddMessagesTable < ActiveRecord::Migration[5.2]
  def self.up
    create_table :messages do |t|
      t.string :original_message, null: false
      t.string :stripped_message, null: false
      t.integer :group_id, null: false
      t.integer :user_id
      t.string :user_name
      t.timestamps

      t.index :stripped_message
      t.index :group_id
    end
  end
  def self.down
    drop_table :messages
  end
end
